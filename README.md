Swap-PnL
========

This python code is used to determine how much PnL (profit and loss) is made on each swap on THORChain. From this data collection, we can calculate how much profit each arb bot is making. This can be used as an approxmation on how much *impermanent loss* the network experiences.

## How it works
To calculate the pnl (profit and loss) of a swap, the code gets the swap
details from THORChain. It queries [Coinpaprika
API](https://coinpaprika.com/api/) to get the price of rune and other asset
and calculates the USD spent or gained by the USD price of the inbound asset
total price vs the outbound asset total price.

This won't be 100% accurate, but should be a decent approximation. But this
should give a sense of how much liquidity is leaving the network due to
arbitration bots.

## API

Some important **NOTES**
1) When submitting a `pool` url param, make sure its valid. Use `/pools` to
get a list of valid pools.
2) When using `start` or `end` url params, use epoch UTC
3) Limit is hard coded to be 50 at max.

### /ping
A healthcheck endpoint, always returns `pong`
```
pong
```

### /height
Returns the current height the chain has processed, and the latest height that
the chain is operating on. You can figure out how far behind the service is
from the tip of the chain from this data.

```
{
  "current_height": <current height swap-pnl has processed>,
  "latest_height": <chain tip height>
}
```

### /pools
List pools

```
[
    "BNB.BNB"
    "BNB.TWT-8C2"
]
```

### /address/<address>?pool=<asset>&start=<start>&end=<end>
Get stats on specific address

```
{
    "count": <num of swaps>,
    "pnl": <pnl>,
}
```

### /swaps/leaderboard?start=<start>&end=<end>
Get a leaderboard of the most profitable arbitration bots

```
{
[
  {
    "address": <address>,
    "count": <num of swaps>,
    "pnl": <pnl>
  },
]
}
```

### /roi?pool=<asset>&start=<start>&end=<end>
Get the total profit or loss of swaps on the network or to a specific pool 

```
[
  {
    "count": <num of swaps>,
    "pnl": <pnl>
  }
]
```

### /swap/<hash_id>
Get the data on a specific swap by hash id

```
{
  "address": <address>,
  "hash": <hash_id>,
  "height": <block height>,
  "pnl": <pnl>,
  "pool": <pool>,
  "ts": <timestamp>
}
```

### /swaps?offset=<offset>&<limit>&address=<address>&pool=<asset>&start=<start>&end=<end>
List swaps

```
[
  {
    "address": <address>,
    "hash": <hash_id>,
    "height": <block height>,
    "pnl": <pnl>,
    "pool": <pool>,
    "ts": <timestamp>
  }
]
```

### /swaps/graph?address=<address>&pool=<asset>&start=<start>&end=<end>&interval=<interval>
Get graph data for swaps. If you supply `interval` format it like so...

```
1 hour
5 minutes
1 day
```

```
[
  {
    "count": <num of swaps>,
    "pnl": <pnl>,
    "time": <timestamp>
  }
]
```

## Development

To run it locally,
```
make start
```

To run linting...
```
make format lint
```

### Testing
No tests were written for this quick tool
